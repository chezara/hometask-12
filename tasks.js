'use strict';
/* 
  Вам необходимо добавить обработчики событий в соотвествии с заданием.
  Вам уже даны готовые стили и разметка. Менять их не нужно,
  за исключением навешивания обработчиков и добавления data - аттрибутов

*/
/*Задание 1
  Необходимо сделать выпадающее меню.
  Меню должно открываться при клике на кнопку,
  закрываться при клике на кнопку, при клике по пункту меню
*/
const menuBtn = document.querySelector('.menu-btn');
const menu = document.querySelector('.menu');
const menuItems = document.querySelectorAll('.menu-item');
let isSelectOpen = false;

const eventMenu = function() {
  if (isSelectOpen === false) {
    menu.classList.add('menu-opened');
    isSelectOpen = true;
  } else {
    menu.classList.remove('menu-opened');
    isSelectOpen = false;
  }
}

menuBtn.addEventListener('click', eventMenu);
for (let i = 0; i < menuItems.length; i++) {
  menuItems[i].addEventListener('click', eventMenu);
}


/**
 * Задание 2
 * Необходимо реализовать перемещение блока по клику на поле.
 * Блок должен перемещаться в место клика.
 * При выполнении задания вам может помочь свойство element.getBoundingClientRect()
 */

const field = document.querySelector('.field');
const movedBlock = document.querySelector('.moved-block');

field.onclick = function(event) {
  const X = event.offsetX;
  const Y = event.offsetY;
  
  movedBlock.style.top = Y + 'px';
  movedBlock.style.left = X + 'px';
}

/**
 * Задание 3
 * Необходимо реализовать скрытие сообщения при клике на крестик при помощи делегирования
 */

const messager = document.querySelector('.messager-container');

messager.onclick = function(event) {
  const { target } = event;
  if (target.classList.contains('remove')) {
    const message = target.parentElement;
    message.remove();
  }
}

/**
 * Задание 4
 * Необходимо реализовать вывод сообщения при клике на ссылку.
 * В сообщении спрашивать, что пользователь точно хочет перейти по указанной ссылке.
 * В зависимости от ответа редиректить или не редиректить пользователя 
 */

 const links = document.querySelectorAll('.link');

for (let i = 0; i < links.length; i++) {
  links[i].onclick = function(event) {
    event.preventDefault();
    var isSure = confirm("Вы уверены, что хотите перейти по ссылке?");

    if (isSure) {
      const { target } = event;
      location.assign(target.getAttribute('href'));
    }
  }
}

  /**
  * Задание 5
  * Необходимо сделать так, чтобы значение заголовка изменялось в соотвествии с измением
  * значения в input
  */
 var input = document.getElementById('fieldHeader');

  input.oninput = function() {
    document.getElementById('taskHeader').innerHTML = input.value;
  };
